//			Nataniel Gaasoy, 131390
//			16HBPROGA
//			imt3103 - Introduction to artificial intelligence

#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <SFML/System.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>
#include <functional>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

const int WINDOWWIDTH = 350;
const int WINDOWLENGTH = 150;
const int BORDERSIZE = 1;
const int SHAPESIZE = 15;
const int GRIDSIZE = 32;
const int GRIDWIDTH = 20 + 2;   //  +2 for border
const int GRIDHEIGHT = 7 + 2;   //  +2 for border

float abs(sf::Vector2f v)
{
    return (sqrt(v.x*v.x + v.y*v.y));
}

float manhattanDistance(sf::Vector2i v, sf::Vector2i w)
{
    float Dy = abs(v.y - w.y);     //  current - goal
    float Dx = abs(v.x - w.x);     //  current - goal
    return Dx + Dy;
}

float heuristicDistance(sf::Vector2i v, sf::Vector2i w)
{
    float Dx = abs(v.x - w.x);     //  current - goal
    float Dy = abs(v.y - w.y);     //  current - goal

    return (sqrt(Dx*Dx + Dy*Dy));
}

class Tile
{
public:
    bool transversable = true;
    bool open = false;
    bool closed = false;
    bool goal = false;
    bool start = false;
    bool path = false;
    sf::Vector2i pos;
    Tile* parent = NULL;
    float cost = 1;
    float g;    //  g(n) is the cost of the path from the start node to n
    float h;    //  h(n) is a heuristic function that estimates the cost of the cheapest path from n to the goal
    float f;    //  f(n)=g(n)+h(n)
    void draw(sf::RenderWindow& w);
    float shapeSize;
};

class Tilemap
{
public:
    Tile tiles[GRIDWIDTH][GRIDHEIGHT];
    char map[20][7];
    sf::Vector2i start;
    sf::Vector2i goal;

    Tilemap();
    void draw(sf::RenderWindow& w);
    void updateTraversability(int Mx, int My);
    void updateGoal(int Mx, int My);
    void updateStart(int Mx, int My);
    void AStar(sf::RenderWindow& w);
    void loadMap();
};

void Tile::draw(sf::RenderWindow& w)
{

    sf::RectangleShape shape;              //  shape
    shape.setSize(sf::Vector2f {13-BORDERSIZE, 13-BORDERSIZE});
    shape.setPosition(pos.x * SHAPESIZE, pos.y * SHAPESIZE);
    shape.setOutlineColor(sf::Color::Black);

    if (!transversable)
    {
        shape.setFillColor(sf::Color::Black);
    }
    else if (path)
    {
        shape.setFillColor(sf::Color::Cyan);
    }
    else if (closed)
    {
        shape.setFillColor(sf::Color::Red);
    }
    else if (open)
    {
        shape.setFillColor(sf::Color::Green);
    }
    else if (goal)
    {
        shape.setFillColor(sf::Color::Blue);
    }
    else if (start)
    {
        shape.setFillColor(sf::Color::Yellow);
    }
    else
    {
        shape.setFillColor(sf::Color::White);
    }
    w.draw(shape);
}

Tilemap::Tilemap()
{
    //  for default map setup
    for (int x = 0; x < GRIDWIDTH; x++)
    {
        for (int y = 0; y < GRIDHEIGHT; y++)
        {
            //  set all tiles traversible
            tiles[x][y].transversable = true;
            tiles[x][y].open = false;
            tiles[x][y].closed = false;
            tiles[x][y].g = 0;
            tiles[x][y].f = 0;
            tiles[x][y].h = 0;

            //  save position of each tile
            tiles[x][y].pos.y = y;
            tiles[x][y].pos.x = x;

            //  border
            tiles[0][y].transversable = false;
            tiles[21][y].transversable = false;
            tiles[x][0].transversable = false;
            tiles[x][8].transversable = false;
        }
    }

    //  set start and goal position
    Tilemap::start = {1,1};
    tiles[1][1].start = true;
    Tilemap::goal = {20, 7};
    tiles[20][7].goal = true;

    //for reading from file
    //Get map:
    std::ifstream inFile;
    std::string mapFile;
    std::cout<<"\nPlease state map name, i.e <1-3>. \n";
    std::cin>>mapFile;  //read map number from input
    inFile.open("../boards/board-"+mapFile+".txt");

    //  reverse x, y in order to print it correctly to the terminal
    for (int y = 0; y < GRIDHEIGHT-2; y++)
    {
        for (int x = 0; x < GRIDWIDTH-2; x++)
        {
            inFile >> map[y][x];
            //debug, print loaded map in terminal
            printf("%c", map[y][x]);

            if (map[y][x] == '.')
            {
                tiles[x+1][y+1].transversable = true;
            }
            else if (map[y][x] == '#')
            {
                tiles[x+1][y+1].transversable = false;
            }
            else if (map[y][x] == 'A')
            {
                Tilemap::start = {x+1,y+1};
                tiles[1][1].start = false;
                tiles[x+1][y+1].start = true;
            }
            else if (map[y][x] == 'B')
            {
                Tilemap::goal = {x+1,y+1};
                tiles[20][7].goal = false;
                tiles[x+1][y+1].goal = true;
            }
        }
        printf("\n");
    }
    inFile.close();
}

void Tilemap::draw(sf::RenderWindow& w)
{
    for (int x = 0; x < GRIDWIDTH; x++)
    {
        for (int y = 0; y < GRIDHEIGHT; y++)
        {
            tiles[x][y].draw(w);
        }
    }
}

void Tilemap::updateTraversability(int Mx, int My)
{
    tiles[Mx][My].transversable = !tiles[Mx][My].transversable;     //  changes traversability
}

void Tilemap::updateGoal(int Mx, int My)
{
    tiles[goal.x][goal.y].goal = false;
    tiles[Mx][My].goal = true;
    Tilemap::goal = { Mx, My };
}

void Tilemap::updateStart(int Mx, int My)
{
    tiles[start.x][start.y].start = false;
    tiles[Mx][My].start = true;
    Tilemap::start = { Mx, My };
}

void Tilemap::AStar(sf::RenderWindow& w)
{
    sf::Vector2i currentTile;

    int biasModifier = 2;      //  Have to add to 'h' when using manhattan distance

    tiles[start.x][start.y].open = true;

    while (currentTile != goal)
    {
        float largeFValue = 10000;

        //current := the node in openSet having the lowest fScore[] value
        for (int x = 0; x < GRIDWIDTH; x++)
        {
            for (int y = 0; y < GRIDHEIGHT; y++)
            {
                if (largeFValue > tiles[x][y].f &&
                    !tiles[x][y].closed &&
                    tiles[x][y].open)
                {
                    currentTile = tiles[x][y].pos;
                    largeFValue = tiles[x][y].f;
                }
            }
        }

        tiles[currentTile.x][currentTile.y].closed = true;

        //saving the positions of surrounding tiles
        sf::Vector2i north = currentTile + sf::Vector2i{ 0, -1 };
        sf::Vector2i south = currentTile + sf::Vector2i{ 0, 1 };
        sf::Vector2i east = currentTile + sf::Vector2i{ 1, 0 };
        sf::Vector2i west = currentTile + sf::Vector2i{ -1, 0 };

        Tile* northTile = &tiles[north.x][north.y];
        Tile* southTile = &tiles[south.x][south.y];
        Tile* eastTile = &tiles[east.x][east.y];
        Tile* westTile = &tiles[west.x][west.y];

        Tile* directionTile[4] = { northTile, southTile, eastTile, westTile };

        //  check all surrounding tiles
        for (int i = 0; i < 4; i++)
        {
            if (directionTile[i]->transversable)
            {
                int newG = directionTile[i]->cost + tiles[currentTile.x][currentTile.y].g;

                if (!directionTile[i]->closed && !directionTile[i]->open)
                {
                    directionTile[i]->h = heuristicDistance(tiles[directionTile[i]->pos.x][directionTile[i]->pos.y].pos, goal)*biasModifier;
                    directionTile[i]->g = newG;
                    directionTile[i]->f = directionTile[i]->g;// + directionTile[i]->h;

                    directionTile[i]->open = true;                                      //  marking the tested tile as open
                    directionTile[i]->parent = &tiles[currentTile.x][currentTile.y];    //  make current tile parent of tested tile
                }
                else if (directionTile[i]->open && !directionTile[i]->closed && directionTile[i]->g > newG)
                {
                    //  if direction tile is tested and has a higher g value
                    directionTile[i]->g = newG;
                    directionTile[i]->f = directionTile[i]->g;// + directionTile[i]->h;
                    directionTile[i]->parent = &tiles[currentTile.x][currentTile.y];
                }
            }
        }

        w.clear(sf::Color::Black);      //  in order to get the A* motion procedurally
        draw(w);                        //  drawn onto the screen
        w.display();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }

    //  Finding the path after goal has been reached
    Tile tilepath;
    tilepath = tiles[currentTile.x][currentTile.y];

    while (tilepath.parent != NULL)
    {
        tiles[tilepath.pos.x][tilepath.pos.y].path = true;
        tilepath = *tilepath.parent;
    }
    tiles[start.x][start.y].path = true;
}

int main()
{
    Tilemap tiles;
    sf::RenderWindow window(sf::VideoMode(WINDOWWIDTH, WINDOWLENGTH), "A*");

    std::cout << "\n\tPress left mouse button to change trasversability. ";
    std::cout << "\n\tPress middle mouse button to change starting position. ";
    std::cout << "\n\tPress right mouse button to change goal position. ";
    std::cout << "\n\tPress the space bar to start. \n";


    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                window.close();
            }
            else if (event.type == event.MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
            {
                int Mx = sf::Mouse::getPosition(window).x / SHAPESIZE;
                int My = sf::Mouse::getPosition(window).y / SHAPESIZE;
                printf("X - %d, Y - %d     ---   ", sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);

                tiles.updateTraversability(Mx, My);
                printf("Transversability was changed at [%i, %i].\n", Mx, My);
            }
            else if (event.type == event.MouseButtonReleased && event.mouseButton.button == sf::Mouse::Right)
            {
                int Mx = sf::Mouse::getPosition(window).x / SHAPESIZE;
                int My = sf::Mouse::getPosition(window).y / SHAPESIZE;

                tiles.updateGoal(Mx, My);
                printf("Goal was updated to [%i, %i].\n", Mx, My);
            }
            else if (event.type == event.MouseButtonReleased && event.mouseButton.button == sf::Mouse::Middle)
            {
                int Mx = sf::Mouse::getPosition(window).x / SHAPESIZE;
                int My = sf::Mouse::getPosition(window).y / SHAPESIZE;

                tiles.updateStart(Mx, My);
                printf("Start was updated to [%i, %i].\n", Mx, My);
            }
            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            {
                std::cout << "\nStart button pressed, finding path. \n";
                tiles.AStar(window);

                window.clear(sf::Color::Black);
                tiles.draw(window);
                window.display();

                std::cout<< "\nA* completed. Please press <ENTER> to terminate. ";
                //std::cin.get();
                //window.close();
            }
        }

        window.clear(sf::Color::Black);
        tiles.draw(window);
        window.display();
    }
    return 0;
}
