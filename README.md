### AI assignment 2, A* ###

#Running on linux without makefile:
*	move into folder where source file is located
*	g++ -c -std=c++11 <source file name>.cpp 
*	g++ <source file name>.o -o main -lsfml-graphics -lsfml-window -lsfml-system
*	./main

#Running on linux with makefile:
*	move into folder where source file is located
*	make
*	(remember to "make clean" to clean up the object and executable files if you wish to run make again without changing the source code)

#Functionality:
*	left click tiles to change traversability
*	right click tile to change goal
*	middle mouse button click tile to change start
*	press space to start the search algorithm
*	input map number from the board folder for the corresponding task code, i.e. <1-3>

#Notes for task 1, grid with hindrances
*	Green marks an "opened" tile (node), a tile that is being checked
*	Red marks a "closed" tile (node), a tile that has been checked

#Notes for task 3, Dijkstra's algorithm
*	Yellow marks an "opened" tile (node), a tile that is being checked (changed to yellow due to grassland color)
*	Red marks a "closed" tile (node), a tile that has been checked
